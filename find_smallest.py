# Kata: https://www.codewars.com/kata/find-the-smallest

def smallest(n):
    l = [int(i) for i in str(n)]
    ll = [int(i) for i in str(n)]
    
    # Build smallest = r
    smallest = min(l[1:])
    
    # Find the smallest number from the right
    i_s = len(l) - l[::-1].index(smallest) - 1
    while l[i_s-1] == smallest:
        i_s -= 1

    # Find optimal position to insert the smallest number, so we iterate over the list until we 
    # find a bigger entry and we insert the smallest right there
    for i, v in enumerate(l):
        if smallest <= v:
            i_j = i
            break
    l.insert(i_j, l.pop(i_s))
    
    # Workaround for near position, eg. [1,0] -> [0,1], [5,4]->[4,5] because of the kata rules
    # that state that i should be the smallest number possible in comparison to j
    r = [int(''.join([str(i) for i in l])) , i_j, i_s] if i_s - i_j == 1 else [int(''.join([str(i) for i in l])) , i_s, i_j]
    
    # Build smallest, alternate = r2
    test_i = 0
    test_j = len(ll)
    # Iterate over the number and find the first entry that is bigger than the next entry
    # eg: 1234123 will find '4' and this will be the number to be moved
    for i, v in enumerate(ll[1:]):
        if ll[i] > v:
            test_i = i
            test_v = ll[i]
            break
    
    # Find where to move the selected number by putting it before a bigger number than itself
    for i, v in enumerate(ll):
        if test_v < v:
            test_j = i - 1
            break
    
    # In case no bigger number was found it will go to the last position
    if test_j == len(ll):
        test_j -= 1
    # This ensures that the number is inserted in the smallest index possible in the case that
    # the list might contain the same number, eg: 192399999 would normally put the forst 9 in
    # the end of the list, however the while loop will ensure that the insertion index will be
    # right before the first of the last sequence of 9's (after the 3)
    while ll[test_j] == test_v:
        test_j -= 1
        
    ll.insert(test_j, ll.pop(test_i))
    r2 = [int(''.join([str(i) for i in ll])) , test_i, test_j]
    
    # return the smallest value
    return r if r[0] < r2[0] else r2
 