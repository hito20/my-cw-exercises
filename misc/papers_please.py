import re

class Inspector:
    rules = {
        'all_require': [],
        'foreigners_require': None,
        'ars_require': [],
        'allowed_citizens': [],
        'denied_citizens': [],
        'wanted': None,
        'foreigners_require': [],
        'workers_req': [],
    }
    reasons = {
        'welcome_ars': 'Glory to Arstotzka.',
        'no_trouble': 'Cause no trouble.',
        'no_passport': 'Entry denied: missing required passport.',
        'no_work_pass': 'Entry denied: missing required work pass.',
        'no_access_permit': 'Entry denied: missing required access permit.',
        'no_ID_card': 'Entry denied: missing required ID card.',
        'no_vaccine_cert': 'Entry denied: missing required certificate of vaccination.',
        'id_mismatch': 'Detainment: ID number mismatch.',
        'nation_mismatch': 'Detainment: nationality mismatch.',
        'dob_mismatch': 'Detainment: date of birth mismatch.',
        'name_mismatch': 'Detainment: name mismatch.',
        'wanted': 'Detainment: Entrant is a wanted criminal.',
        'banned': 'Entry denied: citizen of banned nation.',
        'expired_passport': 'Entry denied: passport expired.',
        'expired_access_permit': 'Entry denied: access permit expired.',
        'expired_work_pass': 'Entry denied: work pass expired.',
        'expired_grant_of_asylum': 'Entry denied: grant of asylum expired.',
        'invalid_diplomatic': 'Entry denied: invalid diplomatic authorization.',
        'missing_vaccination': 'Entry denied: missing required vaccination.',
    }

    p_id = {}

    def docs_parser(self, docs):
        name = None
        dob = None
        # self.p_id['nation'] = ''
        if 'passport' in docs:
            d = docs['passport'].split('\n')
            self.p_id['nation'] = d[1].replace('NATION: ', '')
            self.p_id['id'] = d[0].replace('ID#: ', '')
            name = d[2].replace('NAME: ', '')
            dob = d[3].replace('DOB: ', '')
            if name in self.rules['wanted']:
                return 'wanted'
            defaults = {
                'id': d[0].replace('ID#: ', ''),
                'nation': d[1].replace('NATION: ', ''),
                'name': d[2].replace('NAME: ', ''),
                'dob': d[3].replace('DOB: ', ''),
                'sex': d[4].replace('SEX: ', ''),
                'iss': d[5].replace('ISS: ', ''),
                'exp': d[6].replace('EXP: ', '')
            }
            self.p_id['passport'] = defaults
        if 'work_pass' in docs:
            d = docs['work_pass'].split('\n')
            if name:
                if name != d[0].replace('NAME: ', ''):
                    return 'name_mismatch'
            else:
                name = d[0].replace('NAME: ', '')
                if name in self.rules['wanted']:
                    return 'wanted'
            self.p_id['work_pass'] = {'exp': d[2].replace('EXP: ', '')}
            
        if 'acces_permit' in docs:
            d = docs['acces_permit'].split('\n')
            self.p_id['nation'] = d[1].replace('NATION: ', '')
            self.p_id['id'] = d[2].replace('ID#: ', '')
            
            if name:
                if name != d[0].replace('NAME: ', ''):
                    return 'name_mismatch'
            else:
                name = d[0].replace('NAME: ', '')
                if name in self.rules['wanted']:
                    return 'wanted'
            defaults = {
                'name': d[0].replace('NAME: ', ''),
                'nation': d[1].replace('NATION: ', ''),
                'id': d[2].replace('ID#: ', ''),
                'purpose': d[3].replace('PURPOSE: ', ''),
                'duration': d[4].replace('DURATION: ', ''),
                'height': d[5].replace('HEIGHT: ', ''),
                'weight': d[6].replace('WEIGHT: ', ''),
                'exp': d[7].replace('EXP: ', '')
            }
            self.p_id['acces_permit'] = defaults
        if 'certificate_of_vaccination' in docs:
            d = docs['certificate_of_vaccination'].split('\n')
            if name:
                if name != d[0].replace('NAME: ', ''):
                    return 'name_mismatch'
            else:
                name = d[0].replace('NAME: ', '')
                if name in self.rules['wanted']:
                    return 'wanted'
            self.p_id['certificate_of_vaccination'] = {
                'name': d[0].replace('NAME: ', ''),
                'id': d[1].replace('ID#: ', ''),
                'vaccines': d[2].replace('VACCINES: ', '').replace(', ', ',').split(','),
            }
        if 'ID_card' in docs:
            d = docs['ID_card'].split('\n')
            if name:
                if dob:
                    if dob != d[1].replace('DOB: ', ''):
                        return 'dob_mismatch'
                else:
                    dob = d[1].replace('DOB: ', '')
                if name != d[0].replace('NAME: ', ''):
                    return 'name_mismatch'
            else:
                name = d[0].replace('NAME: ', '')
                if name in self.rules['wanted']:
                    return 'wanted'
            self.p_id['ID_card'] = {
                'name': d[0].replace('NAME: ', ''),
                'dob': d[1].replace('DOB: ', ''),
            }
        if 'grant_of_asylum' in docs:
            d = docs['grant_of_asylum'].split('\n')
            if name:
                if name != d[0].replace('NAME: ', ''):
                    return 'name_mismatch'
            else:
                name = d[0].replace('NAME: ', '')
                if name in self.rules['wanted']:
                    return 'wanted'
            if len(d) == 7:
                if dob:
                    if dob != d[3].replace('DOB: ', ''):
                        return 'dob_mismatch'
                self.p_id['nation'] = d[1].replace('NATION: ', '')
                self.p_id['id'] = d[2].replace('ID#: ', '')
                defaults = {
                    'name': d[0].replace('NAME: ', ''),
                    'nation': d[1].replace('NATION: ', ''),
                    'id': d[2].replace('ID#: ', ''),
                    'dob': d[3].replace('DOB: ', ''),
                    'height': d[4].replace('HEIGHT: ', ''),
                    'weight': d[5].replace('WEIGHT: ', ''),
                    'exp': d[6].replace('EXP: ', '')
                }
            else:
                self.p_id['nation'] = d[1].replace('NATION: ', '')
                self.p_id['id'] = d[2].replace('ID#: ', '')
                defaults = {
                    'name': d[0].replace('NAME: ', ''),
                    'nation': d[1].replace('NATION: ', ''),
                    'id': d[2].replace('ID#: ', ''),
                    'height': d[3].replace('HEIGHT: ', ''),
                    'weight': d[4].replace('WEIGHT: ', ''),
                    'exp': d[5].replace('EXP: ', '')
                }
            self.p_id['grant_of_asylum'] = defaults
        
        b = self.check_nation()
        if not b:
            return 'nation_mismatch'
        b = self.check_ids()
        if not b:
            return 'id_mismatch'
        if 'access_permit' in docs:
            d = docs['access_permit'].split('\n')
            self.p_id['nation'] = d[1].replace('NATION: ', '')
            self.p_id['id'] = d[2].replace('ID#: ', '')
            if name:
                if name != d[0].replace('NAME: ', ''):
                    return 'name_mismatch'
            else:
                name = d[0].replace('NAME: ', '')
                if name in self.rules['wanted']:
                    return 'wanted'
            defaults = {
                'name': d[0].replace('NAME: ', ''),
                'nation': d[1].replace('NATION: ', ''),
                'id': d[2].replace('ID#: ', ''),
                'purpose': d[3].replace('PURPOSE: ', ''),
                'duration': d[4].replace('DURATION: ', ''),
                'height': d[5].replace('HEIGHT: ', ''),
                'weight': d[6].replace('WEIGHT: ', ''),
                'exp': d[7].replace('EXP: ', '')
            }
            self.p_id['access_permit'] = defaults
            b = self.check_nation()
            if not b:
                return 'nation_mismatch'
            if defaults['purpose'] == 'WORK':
                if self.rules['workers_req'] and 'work pass' in self.rules['workers_req']:
                    if 'work_pass' not in docs:
                        return 'no_work_pass'
        if 'diplomatic_authorization' in docs:
            d = docs['diplomatic_authorization'].split('\n')
            if 'id' in self.p_id:
                if self.p_id['id'] != d[2].replace('ID#: ', ''):
                    return 'id_mismatch'
            if 'nation' in self.p_id and self.p_id['nation'] != '':
                if self.p_id['nation'] != d[1].replace('NATION: ', ''):
                    return 'nation_mismatch'
            if name:
                if name != d[0].replace('NAME: ', ''):
                    return 'name_mismatch'
            else:
                name = d[0].replace('NAME: ', '')
                if name in self.rules['wanted']:
                    return 'wanted'
            if 'Arstotzka' not in d[3].replace('ACCESS: ', '').replace(' ', '').split(','):
                return 'invalid_diplomatic'
            self.p_id['diplomatic_authorization'] = {'access': 'granted'}
        print(self.p_id)
        
    def check_ids(self):
        if 'id' not in self.p_id:
            return True
        c_id = self.p_id['id']
        for k in self.p_id:
            if 'id' in self.p_id[k]:
                if c_id != self.p_id[k]['id']:
                    return False
        return True
    def check_nation(self):
        if 'nation' not in self.p_id:
            return True
        nation = self.p_id['nation']
        for k in self.p_id:
            if 'nation' in self.p_id[k]:
                if nation != self.p_id[k]['nation']:
                    return False
        return True
    def check_expiration(self):
        for k in self.p_id:
            if 'exp' in self.p_id[k]:
                if int(self.p_id[k]['exp'].replace('.', '')) < 19821123:
                    return k
                    # print(self.p_id[k]['exp'])
        return None
    def check_wanted(self):
        if self.rules['wanted']:
            for k in self.p_id:
                if 'name' in self.p_id[k]:
                    if self.p_id[k]['name'] in self.rules['wanted']:
                        return True        
        return False
        
    def receive_bulletin(self, bulletin):
        for rule in bulletin.split('\n'):
            print(rule)
            if 'Citizens of Arstotzka require ' in rule:
                items = rule.replace('Citizens of Arstotzka require ' ,'').replace(', ', ',').strip()                
                self.rules['ars_require'].append(items)
            if 'Entrants require' in rule:
                items = rule.replace('Entrants require' ,'').replace(', ', ',').strip()
                self.rules['all_require'].append(items)
            if 'Deny citizens of ' in rule:
                items = rule.replace('Deny citizens of ' ,'').replace(', ', ',').strip()
                for i in items.split(','):
                    if i in self.rules['allowed_citizens']: self.rules['allowed_citizens'].remove(i)
                self.rules['denied_citizens'].extend(items.split(','))
            if 'Allow citizens of ' in rule:
                items = rule.replace('Allow citizens of ' ,'').replace(', ', ',').strip()
                for i in items.split(','):
                    if i in self.rules['denied_citizens']: self.rules['denied_citizens'].remove(i)
                self.rules['allowed_citizens'].extend(items.split(','))
                # self.rules['allowed_citizens'].append('Arstotzka')
            if 'Wanted by the State: ' in rule:
                n = rule.replace('Wanted by the State: ', '')
                self.rules['wanted'] = [n, ', '.join(list(reversed(n.split())))]
            if 'Foreigners require ' in rule:
                items = rule.replace('Foreigners require' ,'').replace(', ', ',').strip()
                self.rules['foreigners_require'].append(items)
            if 'Entrants no longer require ' in rule:
                items = rule.replace('Entrants no longer require ' ,'').replace(', ', ',').strip()
                for i in items.split(','):
                    if i in self.rules['all_require']: self.rules['all_require'].remove(i)
            if 'Workers require ' in rule:
                items = rule.replace('Workers require ' ,'').replace(', ', ',').strip()
                self.rules['workers_req'].extend(items.split(','))
            if 'Foreigners no longer require ' in rule:
                items = rule.replace('Foreigners no longer require ' ,'').replace(', ', ',').strip()
                for i in items.split(','):
                    if i in self.rules['foreigners_require']: self.rules['foreigners_require'].remove(i)
                
        print(self.rules)
        
    def inspect(self, citizen):
        print('citizen: {}'.format(citizen))
        self.p_id = {}
        # name mismatch
        b = self.docs_parser(citizen)
        if b:
            return self.reasons[b]
        if self.check_wanted():
            return self.reasons['wanted']
        b = self.check_ids()
        if not b:
            return self.reasons['id_mismatch']
        b = self.check_nation()
        if not b:
            return self.reasons['nation_mismatch']
        b = self.check_expiration()
        if b:
            return self.reasons['expired_' + b]

        if 'diplomatic_authorization' not in self.p_id:
            if self.rules['allowed_citizens']:
                if 'nation' in self.p_id and self.p_id['nation'] not in self.rules['allowed_citizens']:
                    return self.reasons['banned']
                if 'nation' in self.p_id and self.p_id['nation'] in self.rules['denied_citizens']:
                    return self.reasons['banned']
            if self.rules['all_require']:
                items = self.rules['all_require']
                for item in items:
                    if 'vaccination' in item:
                        i = item.replace(' vaccination', '')
                        if 'certificate_of_vaccination' not in self.p_id:
                            return self.reasons['no_vaccine_cert'] # 'NO VACCINE CERT'
                        if i not in self.p_id['certificate_of_vaccination']['vaccines']:
                            return self.reasons['missing_vaccination']
                    elif item not in citizen:
                        if 'no_'+item in self.reasons:
                            return self.reasons['no_'+item]
                        else:
                            print()
                            print(item)
                            print(self.rules)
                            return 'a'
            if self.rules['ars_require']:
                if self.p_id['nation'] == 'Arstotzka':
                    items = self.rules['ars_require']
                    for item in items:
                        if 'vaccination' in item:
                            i = item.replace(' vaccination', '')
                            if 'certificate_of_vaccination' not in self.p_id:
                                return self.reasons['no_vaccine_cert'] # 'NO VACCINE CERT'
                            if i not in self.p_id['certificate_of_vaccination']['vaccines']:
                                return self.reasons['missing_vaccination']
                        elif item.replace(' ', '_') not in citizen:
                            if 'grant_of_asylum' in self.p_id or 'diplomatic_authorization' in self.p_id:
                                break
                            elif 'no_'+item.replace(' ', '_') in self.reasons:
                                return self.reasons['no_'+item.replace(' ', '_')]
                            else:
                                print()
                                print(item)
                                print(self.reasons)
                                print(self.rules)
                                return 'b'
            if self.rules['foreigners_require']:
                if self.p_id['nation'] != 'Arstotzka':
                    items = self.rules['foreigners_require']
                    for item in items:
                        if 'vaccination' in item:
                            i = item.replace(' vaccination', '')
                            if 'certificate_of_vaccination' not in self.p_id:
                                return self.reasons['no_vaccine_cert'] # 'NO VACCINE CERT'
                            if i not in self.p_id['certificate_of_vaccination']['vaccines']:
                                return self.reasons['missing_vaccination']
                        elif item.replace(' ', '_') not in citizen:
                            if 'grant_of_asylum' in self.p_id or 'diplomatic_authorization' in self.p_id:
                                break
                            elif 'no_'+item.replace(' ', '_') in self.reasons:
                                return self.reasons['no_'+item.replace(' ', '_')]
                            else:
                                print()
                                print(item)
                                print(self.reasons)
                                print(self.rules)
                                return 'b2'

        if self.rules['allowed_citizens']:
            if self.p_id['nation'] not in self.rules['allowed_citizens']:
                return self.reasons['banned']
            if self.p_id['nation'] in self.rules['denied_citizens']:
                return self.reasons['banned']
        if self.p_id['nation'] == 'Arstotzka':
            print()
            print(self.rules)
            return self.reasons['welcome_ars']
        else:
            print()
            print(self.rules)
            return self.reasons['no_trouble']
