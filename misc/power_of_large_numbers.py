# level 3 https://www.codewars.com/kata/5518a860a73e708c0a000027
def solve(lst):
    for i in range(len(lst)-2, -1, -1):
        n1 = lst[i]
        lst[i] = pow(n1, lst[i+1], 10000000)
        if lst[i] == 0 and n1 != 0:
            lst[i] = 10000000
    return lst[0] % 10
def last_digit(lst):
    return 1 if not lst else lst[0] % 10 if len(lst) == 1 else solve(lst)