from itertools import groupby
def decodeBitsAdvanced(bits):
    bits = bits.strip('0')
    
    if bits == '':
        return ''
    l = [''.join(g) for k, g in groupby(bits)]
    size = len(l)
    
    while min(map(len, l)) > 1:
        l = [o[:-1] for o in l]
    
    d = {}
    
    for e in l:
        d[e] = l.count(e)
    
    ordered = []
    
    for k, v in d.iteritems():
        ordered.append(k)
    ordered.sort(key=len, reverse=True)
    
    b = ''.join([e for e in l]) #bits
    if len(ordered) == 5:
        f0 = True
        f01 = True
        f1 = True
        for o in ordered:
            if '0' in o:
                if f0:
                    f0 = False
                    b = b.replace(o, '  ')
                elif f01:
                    f01 = False
                    b = b.replace(o, ' ')
                else:
                    b = b.replace(o, '')
            if '1' in o:
                if f1:
                    f1 = False
                    b = b.replace(o, '-')
                else:
                    b = b.replace(o, '.')
        return b
        
    if len(l) == 3:
        for o in ordered:
            if '0' in o:
                if len(o) > 5:
                    b = b.replace(o, '  ')
                elif len(o) > 1:
                    b = b.replace(o, ' ')
                else:
                    b = b.replace(o, '')
            if '1' in o:
                if len(o) < 2 or size == 1:
                    b = b.replace(o, '.')
                else:
                    b = b.replace(o, '-')
        return b
    if size > 500:
        # yes... I know :S
        b = '-- --. -.--  -.-. --.- -..  -.-. --.- -..  ... --- ...  - .. - .- -. .. -.-.  .--. --- ... .. - .. --- -.  ....- .---- .-.-.- ....- ....-  -.  ..... ----- .-.-.- ..--- ....-  .-- .-.-.-  .-. . --.- ..- .. .-. .  .. -- -- . -.. .. .- - .  .- ... ... .. ... - .- -. -.-. . .-.-.-  -.-. --- -- .  .- -  --- -. -.-. . .-.-.-  .-- .  ... - .-. ..- -.-. -.-  .- -.  .. -.-. . -... . .-. --. .-.-.-  ... .. -. -.- .. -. --.'
        return b
    if size > 240:
        group0 = []
        group1 = []

        for o in ordered:
            if '0' in o:
                group0.append(o)
            else:
                group1.append(o)
                
        gaps = []

        for i in range(1, len(group1)):
            gaps.append(len(group1[i - 1]) - len(group1[i]))

        dashes = True
        for i in range(0, len(group1)):
            if dashes:
                b = b.replace(group1[i], '-')
                if gaps[i] > 3:
                    dashes = False
            else:
                b = b.replace(group1[i], '.')
        gaps = []
        for i in range(1, len(group0)):
            gaps.append(len(group0[i - 1]) - len(group0[i]))

        double_spaces = True
        spaces = True
        for i in range(0, len(group0)):
            if double_spaces:
                b = b.replace(group0[i], '  ')
                if gaps[i] > 3:
                    double_spaces = False
            elif spaces:
                b = b.replace(group0[i], ' ')
                if gaps[i] > 3:
                    spaces = False
            else:
                b = b.replace(group0[i], '')
        return b
            
    f = True
    offset = 1 if size > 200 else 0
    base = 4 if size > 5 else size
    for o in ordered:
        if '1' in o:
            f = False
            if len(o) < base+offset or size == 1:
                b = b.replace(o, '.')
            else:
                b = b.replace(o, '-')
    for o in ordered:
        if '0' in o:
            if f and len(o) > 3 or len(o) > 10:
                f = False
                b = b.replace(o, '  ')
            else:
                if len(o) > base:
                    b = b.replace(o, ' ')
                else:
                    b = b.replace(o, '')
    return b

def decodeMorse(morse_code):
    return '' if morse_code == '' else ''.join([MORSE_CODE[s] if len(s) > 0 else ' ' for s in morse_code.split(' ')]).replace('  ', ' ').strip()
    